# babel-preset-epublishing

This module is a preset for [Babel](https://babeljs.io), which bundles all of the plugins and presets necessary to build [ePublishing](https://www.epublishing.com) front-end assets.

## Installation

```sh
npm install [--save|--save-dev] babel-preset-epublishing
```

## Usage

In .babelrc:

```json
{
  "presets": [ "epublishing" ]
}
```

As a JS object in Grunt config:

```js
{
  babel: {
    options: {
      presets: [ 'epublishing' ],
    },
  },
}
```

With Webpack v2 via `babel-loader`:

```js
{
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: {
          presets: [ 'epublishing' ],
        },
      },
    ],
  },
}
```

## Configuration

Custom options can be passed to this preset in the usual way:

```json
{
  "presets": [
    ["epublishing", {
      "env": {
        "modules": "amd"
      }
    }]
  ]
}
```

### Default Configuration Object

If an options object is passed to the preset, it is merged with ePublishing's default configuration, and the resulting values are passed down to individual presets and plugins.

```js
{
  env: {
    modules: 'umd',
    targets: {
      browsers: [ 'last 3 versions', 'ie 11' ],
      uglify: true
    }
  },
  lodash: {
    // It's possible to override the option below, but it's usually set
    // dynamically by grunt-jade at compile time:
    cwd: undefined
  },
  transformRuntime: true, // set to false to disable babel-plugin-transform-runtime
}
```

`babel-preset-env` accepts many additional options that we don't currently use. See the [babel-preset-env docs](http://babeljs.io/docs/plugins/preset-env/#options) to find out what's available.
