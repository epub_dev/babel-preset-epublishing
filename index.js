const defaults = {
  env: {
    modules: "umd",
    targets: {
      browsers: ["last 3 versions"],
      uglify: true,
    },
  },
  transformRuntime: true,
  lodash: {},
  minify: false,
};

module.exports = function buildEpublishingPreset(context, options) {
  options = options || {};
  const config = Object.assign({}, defaults, options);
  const optionalPlugins = [];
  const presets = ["@babel/preset-env", "@babel/preset-react"];

  if (config.transformRuntime) {
    optionalPlugins.push("@babel/plugin-syntax-dynamic-import");
  }

  if (config.minify) {
    presets.push("minify");
  }

  const lodashPlugin = [require("babel-plugin-lodash"), config.lodash];

  const plugins = [
    lodashPlugin,
    [
      "@babel/plugin-transform-react-jsx",
      {
        "pragma": "h",
        "pragmaFrag": "Fragment"
      }
    ]
  ].concat(optionalPlugins);

  return {
    presets,
    plugins,
  };
};
